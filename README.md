# ComputerCraft Linux

This is a "Linux" made in Minecraft using computercraft mod.

(This isn't a real Linux. It only tries to look like a Linux.)

**Base commands:**
* apt
* cat
* cd
* clear
* cp
* echo
* ip
* ls
* mkdir
* mv
* nano
* pwd
* reboot
* rm
* rmdir
* shutdown
* sudo
* touch
* uname
* useradd (Not finished)
* whoami

**About apt command**

Apt command is directed to this GitLab repository. In a folder named "apts" there are all custom commands that can be downloaded using this command.