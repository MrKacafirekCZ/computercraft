function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

r = false

for _, opt in ipairs(args) do
    if starts_with(opt, "-") then
        if opt == "-r" then
            r = true
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w <= 1 then
    return "Error:Not enough parameters!"
elseif #args_w >= 3 then
    return "Error:Too many parameters!"
end

from = "/linux"..directory.."/"..args_w[1]
to = "/linux"..directory.."/"..args_w[2]

if starts_with(args_w[1], "/") then
    from = "/linux"..args_w[1]
elseif starts_with(args_w[1], "~") then
    if user == "root" then
        from = "/linux/root/"..args_w[1]
    else
        from = "/linux/home/"..user.."/"..args_w[1]
    end
end

if starts_with(args_w[2], "/") then
    from = "/linux"..args_w[2]
elseif starts_with(args_w[1], "~") then
    if user == "root" then
        from = "/linux/root/"..args_w[1]:sub(2, #args_w[1])
    else
        from = "/linux/home/"..user.."/"..args_w[1]:sub(2, #args_w[1])
    end
end

if not fs.exists(from) then
    return "Error:File doesn't exist!"
end

if fs.exists(to) then
    return "Error:Destination already exists!"
else
    if fs.isDir(from) then
        if r then
            fs.copy(from, to)
            return "Success!"
        else
            return "Error:File is a directory!"
        end
    else
        fs.copy(from, to)
        return "Success!"
    end
end