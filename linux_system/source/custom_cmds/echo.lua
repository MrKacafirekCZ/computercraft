function split(text, sep, skip)
    local t={}

    if skip == nil then
        if sep == nil then
            sep = "%s"
        end
        
        for str in string.gmatch(text, "([^"..sep.."]+)") do
            if skip == nil then
                table.insert(t, str)
            elseif starts_with(str, skip) then
            else
                table.insert(t, str)
            end
        end
    else
        skipnext = false

        if sep == nil then
            sep = "%s"
        end
        
        for str in string.gmatch(text, "([^"..sep.."]+)") do
            if skipnext == true then
                skipnext = false
            elseif str == ">" then
                skipnext = true
            else
                if skip == nil then
                    table.insert(t, str)
                elseif starts_with(str, skip) then
                else
                    table.insert(t, str)
                end
            end
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function ends_with(str, ending)
    return ending == "" or str:sub(-#ending) == ending
 end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", ">")
args = split(args[1], " ", nil)

right = false
filename = ""

for i, opt in ipairs(args) do
    if starts_with(opt, ">") then
        if opt == ">" then
            right = true
            filename = args[i + 1]
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w < 1 then
    return "Error:Not enough parameters!"
elseif #args_w > 1 then
    return "Error:Too many parameters!"
end

if starts_with(args_w[1], "\"") and ends_with(args_w[1], "\"") or starts_with(args_w[1], "\'") and ends_with(args_w[1], "\'") then
    if right then
        dir = "/linux"..directory.."/"..filename

        if starts_with(filename, "/") then
            dir = "/linux"..filename
        elseif starts_with(filename, "~") then
            if user == "root" then
                dir = "/linux/root"..filename
            else
                dir = "/linux/home/"..user..filename
            end
        end
    
        if fs.exists(dir) then
            if fs.isDir(dir) then
                return "Error:File is a directory!"
            else
                local h = fs.open(dir, "a")
                h.writeLine(args_w[1]:sub(2, #args_w[1] - 1))
                h.flush()
                h.close()
            end
        else
            local h = fs.open(dir, "w")
            h.writeLine(args_w[1]:sub(2, #args_w[1] - 1))
            h.flush()
            h.close()
        end
    end
    
    return "Print:"..args_w[1]:sub(2, #args_w[1] - 1)
else
    return "Error:Invalid use of command!"
end