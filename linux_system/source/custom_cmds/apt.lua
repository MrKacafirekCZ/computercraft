function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function ends_with(str, ending)
    return ending == "" or str:sub(-#ending) == ending
end

function download(file)
    print("Checking connection...")

    local ok, err = http.checkURL("https://gitlab.com/MrKacafirekCZ/computercraft/raw/master/"..file)
    if not ok then
        if err then
            return "Error:"..err
        else
            return "Error:No connection!"
        end
    end

    print("Trying to find "..file:sub(1, #file - 4).."...")

    local response = http.get("https://gitlab.com/MrKacafirekCZ/computercraft/raw/master/"..file, nil, true)
    if not response then
        return "Error:No "..file:sub(1, #file - 4).." found!"
    end

    print("Successfully found and downloaded "..file:sub(1, #file - 4).."!")

    local sResponse = response.readAll()
    response.close()

    if sResponse then
        if args_w[1] == "update" then
            local f = fs.open("/linux/tmp/update.txt", "wb")
            f.write(sResponse)
            f.close()
            return "Success!"
        elseif args_w[1] == "install" then
            local f = fs.open("/custom_cmds/"..file, "wb")
            f.write(sResponse)
            f.close()
            return "Success!"
        end
    end
end

args = {...}

directory = args[2]
user = args[3]
perm_level = args[4]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

if perm_level == 0 then
    return "Error:Permission denied!"
end

if #args_w < 1 then
    return "Error:Not enough parameters!"
elseif #args_w > 2 then
    return "Error:Too many parameters!"    
end

if args_w[1] == "install" and #args_w == 2 then
    if ends_with(args_w[2], ".lua") then
        return "Error:Invalid use of command!"
    else
        return download(args_w[2]..".lua")
    end
elseif args_w[1] == "update" and #args_w == 1 then
    return download("update.txt")
end