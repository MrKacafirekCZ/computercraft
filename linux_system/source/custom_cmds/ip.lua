function split(text, sep)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
args = split(args[1], " ")

if #args == 0 then
    return "Error:Not enough parameters!"
elseif #args >= 2 then
    return "Error:Too many parameters!"
end

if args[1] == "addr" or args[1] == "address" then
    return "Print:1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UKNOWN\n     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00\n     inet 127.0.0.1/8 scope host lo\n        valid_lft forever preferred_lft forever\n     inet6 ::1/128 scope host\n        valid_lft forever preferred_lft forever"
else
    return "Error:Unknown command!"
end