function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function ends_with(str, ending)
    return ending == "" or str:sub(-#ending) == ending
end

function isEmpty(dir)
    if fs.isDir(dir) then
        files = fs.list(dir)

        if #files == 0 then
            return true
        else
            return false
        end
    elseif fs.getSize(dir) > 0 then
        return false
    else
        return true
    end
end

function cmdRm(dir)
    if fs.exists(dir) then
        if fs.isDir(dir) and not d then
            return "Error:File is a directory"
        else
            if r then
                fs.delete(dir)
                return "Success!"
            else
                if isEmpty(dir) then
                    fs.delete(dir)
                    return "Success!"
                else
                    if d then
                        return "Error:Directory isn't empty!"
                    else
                        return "Error:File isn't empty!"
                    end
                end
            end
        end
    else
        return "Error:File doesn't exist!"
    end
end

function getDir(dir)
    new_dir = directory.."/"..dir

    if starts_with(dir, "/") then
        new_dir = "/"..dir
    elseif starts_with(dir, "~") then
        if user == "root" then
            new_dir = "/root/"..dir:sub(3, #dir)
        else
            new_dir = "/home/"..user.."/"..dir:sub(3, #dir)
        end
    end

    new_dir = fs.combine(new_dir, "")

    while starts_with(new_dir, "/..") or starts_with(new_dir, "..") do
        if starts_with(new_dir, "/..") then
            new_dir = new_dir:sub(4, #new_dir)
        elseif starts_with(new_dir, "..") then
            new_dir = new_dir:sub(3, #new_dir)
        end
    end

    if starts_with(new_dir, "/") then
    else
        new_dir = "/"..new_dir
    end

    return "/linux"..new_dir
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

d = false
r = false

for _, opt in ipairs(args) do
    if starts_with(opt, "-") then
        if opt == "-d" then
            d = true
        elseif opt == "-r" then
            r = true
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w == 0 then
    return "Error:Not enough parameters!"
elseif #args_w >= 2 then
    return "Error:Too many parameters!"    
end

if ends_with(split(args_w[1], "/")[#split(args_w[1], "/")], "*") then
    dir = getDir(args_w[1]:sub(1, #args_w[1] - 1))

    if ends_with(dir, "/") then
    else
        dir = dir.."/"
    end

    local FileList = fs.list(dir)

    for _, file in ipairs(FileList) do
        rtn = cmdRm(dir..file)

        if starts_with(rtn, "Error:") then
            return rtn
        end
    end
    return "Success!"
else
    dir = getDir(args_w[1])
    return cmdRm(dir)
end
