function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
cmd = split(args[1], " ", nil)
args = split(args[1], " ", cmd[1])

if not fs.exists("custom_cmds/"..cmd[1]..".lua") then
    return "Error:Command not found!"
end

error = assert(loadfile("custom_cmds/"..cmd[1]..".lua"))(table.concat(args, " "), directory, user, 1)

if starts_with(error, "Error:") then
    return "Error:"..cmd[1]..": "..error:sub(7, #error)
else
    return error
end