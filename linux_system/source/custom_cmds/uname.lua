function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

kernel_name = "Linux"
nodename = "ComputerCraft"
kernel_release = "0.0.1-0-generic"
kernel_version = "#1 SMP Thr Nov 21 16:45:00 UTC 2019"
machine = "x86_64"
processor = "x86_64"
hardware_platform = "x86_64"
operating_system = "GNU/Linux"
text = ""

for _, opt in ipairs(args) do
    if starts_with(opt, "-") then
        if opt == "-a" then
            text = text..kernel_name.." "..nodename.." "..kernel_release.." "..kernel_version.." "..machine.." "..processor.." "..hardware_platform.." "..operating_system
        elseif opt == "-i" then
            text = text..hardware_platform
        elseif opt == "-m" then
            text = text..machine
        elseif opt == "-n" then
            text = text..nodename
        elseif opt == "-o" then
            text = text..operating_system
        elseif opt == "-p" then
            text = text..processor
        elseif opt == "-r" then
            text = text..kernel_release
        elseif opt == "-s" then
            text = text..kernel_name
        elseif opt == "-v" then
            text = text..kernel_version
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w > 0 then
    return "Error:Too many parameters!"
end

if text == "" then
    return "Print:Linux"
else
    return "Print:"..text
end