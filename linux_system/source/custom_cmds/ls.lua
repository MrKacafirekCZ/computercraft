function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function getDir(dir)
    new_dir = directory.."/"..dir

    if starts_with(dir, "/") then
        new_dir = "/"..dir
    elseif starts_with(dir, "~") then
        if user == "root" then
            new_dir = "/root/"..dir:sub(3, #dir)
        else
            new_dir = "/home/"..user.."/"..dir:sub(3, #dir)
        end
    end

    new_dir = fs.combine(new_dir, "")

    while starts_with(new_dir, "/..") or starts_with(new_dir, "..") do
        if starts_with(new_dir, "/..") then
            new_dir = new_dir:sub(4, #new_dir)
        elseif starts_with(new_dir, "..") then
            new_dir = new_dir:sub(3, #new_dir)
        end
    end

    if starts_with(new_dir, "/") then
    else
        new_dir = "/"..new_dir
    end

    return "/linux"..new_dir
end

function ls(dir)
    local FileList = fs.list(dir)
    files = ""

    if a then
        files = "&b..   "
    end

    for _, file in ipairs(FileList) do
        if fs.isDir(dir.."/"..file) then
            files = files.."&b"..file.."   "
        else
            files = files.."&w"..file.."   "
        end
    end

    return "Print:"..files
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

a = false

for _, opt in ipairs(args) do
    if starts_with(opt, "-") then
        if opt == "-a" then
            a = true
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w == 0 then
    return ls("/linux"..directory)
elseif #args_w == 1 then
    dir = getDir(args[1])

    if fs.isDir(dir) then
        return ls(dir)
    else
        return "Error:Not a directory!"
    end
elseif #args_w >= 2 then
    return "Error:Too many parameters!"
end