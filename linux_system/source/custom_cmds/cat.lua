function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function getDir(dir)
    new_dir = directory.."/"..dir

    if starts_with(dir, "/") then
        new_dir = "/"..dir
    elseif starts_with(dir, "~") then
        if user == "root" then
            new_dir = "/root/"..dir:sub(3, #dir)
        else
            new_dir = "/home/"..user.."/"..dir:sub(3, #dir)
        end
    end

    new_dir = fs.combine(new_dir, "")

    while starts_with(new_dir, "/..") or starts_with(new_dir, "..") do
        if starts_with(new_dir, "/..") then
            new_dir = new_dir:sub(4, #new_dir)
        elseif starts_with(new_dir, "..") then
            new_dir = new_dir:sub(3, #new_dir)
        end
    end

    if starts_with(new_dir, "/") then
    else
        new_dir = "/"..new_dir
    end

    return "/linux"..new_dir
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

if #args_w < 1 then
    return "Error:Not enough parameters!"
elseif #args_w > 1 then
    return "Error:Too many parameters!"
end

dir = getDir(args_w[1])

if fs.exists(dir) then
    if fs.isDir(dir) then
        return "Error:File is a directory!"
    else
        local h = io.open(dir, "r")
        line = h:read()
        text = ""
        while line do
            if text == "" then
                text = line
            else
                text = text.."\n "..line
            end
            line = h:read()
        end
        h:close()

        return "Print:"..text
    end
else
    return "Error:File doesn't exist!"
end
