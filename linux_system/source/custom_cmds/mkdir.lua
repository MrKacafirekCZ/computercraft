function split(text, sep)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
perm_level = args[4]
args = split(args[1], " ")

if perm_level == 0 then
    return "Error:Permission denied!"
end

if #args == 0 then
    return "Error:Not enough parameters!"
elseif #args >= 2 then
    return "Error:Too many parameters!"
end

dir = "/linux"..directory.."/"..args[1]

if starts_with(args[1], "/") then
    dir = "/linux"..args[1]
elseif starts_with(args[1], "~") then
    if user == "root" then
        dir = "/linux/root/"..args[1]:sub(2, #args[1])
    else
        dir = "/linux/home/"..user.."/"..args[1]:sub(2, #args[1])
    end
end

if fs.exists(dir) then
    return "Error:Directory exists!"
else
    fs.makeDir(dir)
    return "Success!"
end