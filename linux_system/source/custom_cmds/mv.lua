function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function getDir(dir)
    new_dir = directory.."/"..dir

    if starts_with(dir, "/") then
        new_dir = "/"..dir
    elseif starts_with(dir, "~") then
        if user == "root" then
            new_dir = "/root/"..dir:sub(3, #dir)
        else
            new_dir = "/home/"..user.."/"..dir:sub(3, #dir)
        end
    end

    new_dir = fs.combine(new_dir, "")

    while starts_with(new_dir, "/..") or starts_with(new_dir, "..") do
        if starts_with(new_dir, "/..") then
            new_dir = new_dir:sub(4, #new_dir)
        elseif starts_with(new_dir, "..") then
            new_dir = new_dir:sub(3, #new_dir)
        end
    end

    if starts_with(new_dir, "/") then
    else
        new_dir = "/"..new_dir
    end

    return "/linux"..new_dir
end

args = {...}

directory = args[2]
user = args[3]
perm_level = args[4]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

if perm_level == 0 then
    return "Error:Permission denied!"
end

if #args_w < 2 then
    return "Error:Not enough parameters!"
elseif #args_w > 2 then
    return "Error:Too many parameters!"
end

from = getDir(args[1])
to = getDir(args[2])

if fs.exists(from) then
    fs.move(from, to)

    return "Success!"
else
    return "Error:No such file or directory!"
end