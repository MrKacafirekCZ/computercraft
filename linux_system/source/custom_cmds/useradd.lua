function split(text, sep, skip)
    skipnext = false

    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skipnext == true then
            skipnext = false
        elseif str == "-p" or str == "-k" then
            skipnext = true
        else
            if skip == nil then
                table.insert(t, str)
            elseif starts_with(str, skip) then
            else
                table.insert(t, str)
            end
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

args = {...}

directory = args[2]
user = args[3]
perm_level = args[4]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

if perm_level == 0 then
    return "Error:Permission denied!"
end

k = false
m = false
p = false

for _, opt in ipairs(args) do
    if starts_with(opt, "-") then
        if opt == "-k" then
            k = true
        elseif opt == "-m" then
            m = true
        elseif opt == "-p" then
            p = true
        else
            return "Error:Invalid option!"
        end
    end
end

if #args_w == 0 then
    return "Error:Not enough parameters!"
elseif #args_w >= 2 then
    return "Error:Too many parameters!"
end

return "Error:Comming Soon!"