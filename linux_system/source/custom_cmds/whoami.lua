function split(text, sep)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

args = {...}

directory = args[2]
user = args[3]
args = split(args[1], " ")

if #args == 0 then
    return "Print:"..user
else
    return "Error:Too many parameters!"
end