local fileLines = {}

function split(text, sep, skip)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        if skip == nil then
            table.insert(t, str)
        elseif starts_with(str, skip) then
        else
            table.insert(t, str)
        end
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function getDir(dir)
    new_dir = directory.."/"..dir

    if starts_with(dir, "/") then
        new_dir = "/"..dir
    elseif starts_with(dir, "~") then
        if user == "root" then
            new_dir = "/root/"..dir:sub(3, #dir)
        else
            new_dir = "/home/"..user.."/"..dir:sub(3, #dir)
        end
    end

    new_dir = fs.combine(new_dir, "")

    while starts_with(new_dir, "/..") or starts_with(new_dir, "..") do
        if starts_with(new_dir, "/..") then
            new_dir = new_dir:sub(4, #new_dir)
        elseif starts_with(new_dir, "..") then
            new_dir = new_dir:sub(3, #new_dir)
        end
    end

    if starts_with(new_dir, "/") then
    else
        new_dir = "/"..new_dir
    end

    return "/linux"..new_dir
end

function changeBF(back, fore)
    term.setTextColor(fore)
    term.setBackgroundColor(back)
end

function writeHelp(short, desc)
    changeBF(colors.lightGray, colors.black)
    write(short)
    changeBF(colors.black, colors.white)
    write(desc)
end

function nanoHelp()
    term.setCursorPos(1, 18)
    term.clearLine()
    writeHelp("^O", " WriteOut  ")
    writeHelp("^X", " Exit      ")
    term.setCursorPos(1, 19)
    term.clearLine()
end

function nanoSave()
    term.setCursorPos(1, 18)
    term.clearLine()
    writeHelp(" Y", " Yes       ")
    term.setCursorPos(1, 19)
    term.clearLine()
    writeHelp(" N", " No        ")
    writeHelp("^C", " Cancel    ")
end

function writeFile()
    for i, line in ipairs(fileLines) do
        if i < 14 then
            chars = {}
            line:gsub(".",function(c) table.insert(chars,c) end)

            for j, char in ipairs(chars) do
                if j < 52 then
                    write(char)
                else
                    break
                end
            end
            print("")
        else
            break
        end
    end
end

args = {...}

directory = args[2]
user = args[3]
args_w = split(args[1], " ", "-")
args = split(args[1], " ", nil)

if #args_w < 1 then
    return "Error:Not enough parameters!"
elseif #args_w > 1 then
    return "Error:Too many parameters!"
end

dir = getDir(args_w[1])

if fs.exists(dir) then
    if fs.isDir(dir) then
        return "Error:File is a directory!"
    else
        local h = io.open(dir, "r")
        line = h:read()
        lines = 0
        while line do
            table.insert(fileLines, line)
            lines = lines + 1
            line = h:read()
        end
        h:close()

        term.clear()
        term.setCursorPos(1, 1)
        changeBF(colors.lightGray, colors.black)
        write(" GNU nano 0.0.1   File: "..fs.getName(dir))
        for i = 27 - #fs.getName(dir), 1, -1 do
            write(" ")
        end
        changeBF(colors.black, colors.white)

        nano = true
        menu = 0

        while nano do
            term.setCursorPos(1, 3)
            writeFile()

            term.setCursorPos(1, 17)
            term.clearLine()
            if menu == 0 then
                write("                 ")
                changeBF(colors.lightGray, colors.black)
                write("[ Read "..lines.." lines ]")
                changeBF(colors.black, colors.white)
                nanoHelp()
            else
                changeBF(colors.lightGray, colors.black)
                write("Save modified buffer?                    ")
                changeBF(colors.black, colors.white)
                nanoSave()
            end

            lastKey = 0
            while true do
                local event, key, isHeld = os.pullEvent("key")
                if lastKey == 29 and key == 45 and menu == 0 then
                    nano = false
                    break
                elseif lastKey == 29 and key == 46 and menu == 1 then
                    menu = 0
                    break
                elseif lastKey == 29 and key == 24 and menu == 0 then
                    menu = 1
                    break
                elseif key == 28 and menu == 1 then
                    menu = 0
                    break
                end

                --if key == 28 then
                    --shut =true
				    --break
			    --elseif key == 14 then
                    --backspace()
			    --elseif disabledKeys(key) == true then
                --else
                    --local event, char = os.pullEvent("char")
                    --monitor.write("test")
                    --writeLetter(key, char)
                --end

                lastKey = key
            end
        end

        return "Success!"
    end
else
    return "Error:File doesn't exist!"
end
