local monitor = peripheral.wrap("top")
local user = "user"
local directory = "/home/user"
local dir = ""
local shutdown_bool = false
local command = ""
local perm_level = 0

function backspace()
    if #command == 0 then
    else
        local x, y = monitor.getCursorPos()
        monitor.setCursorPos(x - 1, y)
        monitor.write(" ")
        monitor.setCursorPos(x - 1, y)

        x, y = term.getCursorPos()
        term.setCursorPos(x - 1, y)
        term.write(" ")
        term.setCursorPos(x - 1, y)

        command = string.sub(command, 1, #command - 1)
    end
end

function writeLetter(key, char)
	if disabledKeys(key) == true then
	else
		if key == 57 then
            write_f(" ")
            write(" ")
			command = command.." "
		elseif key == 28 then
		elseif key == 14 then
		else
            write_f(char)
            write(char)
			command = command..char
		end
	end
end

function disabledKeys(key)
	bool = true
	
	if key == 29 then
	elseif key == 219 then
	elseif key == 56 then
	elseif key == 184 then
	elseif key == 220 then
	elseif key == 221 then
	elseif key == 157 then
	elseif key == 54 then
	elseif key == 15 then
	elseif key == 58 then
	elseif key == 42 then
	elseif key == 200 then
	elseif key == 203 then
	elseif key == 208 then
	elseif key == 205 then
	elseif key == 69 then
	elseif key == 1 then
	elseif key == 59 then
	elseif key == 60 then
	elseif key == 61 then
	elseif key == 62 then
	elseif key == 63 then
	elseif key == 64 then
	elseif key == 65 then
	elseif key == 66 then
	elseif key == 67 then
	elseif key == 68 then
	elseif key == 87 then
	elseif key == 88 then
	elseif key == 183 then
	elseif key == 70 then
	elseif key == 197 then
	elseif key == 210 then
	elseif key == 199 then
	elseif key == 201 then
	elseif key == 211 then
	elseif key == 207 then
	elseif key == 209 then
	else
		bool = false
	end
	
	return bool
end

function print_f(text)
    write_f(text)

    print("")
    term.redirect(monitor)
    print("")
    term.redirect(term.native())
end

function write_f(text)
    if #text > 1 then
        if string.sub(text, 1, 1) == "&" then
            text = " "..text
        end

        local t = split(text, "&")

        if t == nil then
            write_str(text)
        else
            for str in pairs(t) do
                if str == 1 then
                    write_str(t[str])
                else
                    txt = t[str]
                    char = string.sub(txt, 1, 1)

                    color = nil
                    if char == "g" then
                        color = colors.green
                    elseif char == "r" then
                        color = colors.red
                    elseif char == "w" then
                        color = colors.white
                    elseif char == "o" then
                        color = colors.orange
                    elseif char == "b" then
                        color = colors.lightBlue
                    end
                
                    change_color(color)
                    strng = string.sub(txt, 2, txt.length)
                    write_str(strng)
                end
            end
        end
    else
        write_str(text)
    end
end

function write_str(text)
    local t = split(text, "\n")
    tamount = #t

    for str in pairs(t) do
        monitor.write(t[str])

        if tamount > 1 then
            print("")
            term.redirect(monitor)
            print("")
            term.redirect(term.native())
            tamount = tamount - 1
        end
    end
end

function change_color(color)
    monitor.setTextColor(color)
    term.setTextColor(color)
end

function split(text, sep)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(text, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

function consolePrefix()
    term.clear()
    term.setCursorPos(1, 1)
    write_f("&g"..user.."@computercraft:&b"..dir.."&w$ ")
    write("$ ")
end

function change_directory(new_dir)
    directory = new_dir



    if starts_with(new_dir, "/home/"..user) and user ~= "root" then
        dir = "~"..new_dir:sub(7 + #user, #new_dir)
    elseif starts_with(new_dir, "/root") and user == "root" then
        dir = "~"..new_dir:sub(6, #new_dir)
    else
        dir = new_dir
    end
end

function commandExecute()
    cmds2 = split(command, "&&")
    cmds = {}

    for _, com in ipairs(cmds2) do
        cmds3 = split(com, ";")

        for _, cmd in ipairs(cmds3) do
            table.insert(cmds, cmd)
        end
    end

    for _, com in ipairs(cmds) do
        if starts_with(com, " ") then
            com = com:sub(2, #com)    
        end

        cmd = split(com, " ")
        args = ""

        if cmd[1] == nil then
            cmd[1] = com
        else
            args = com:sub(#cmd[1] + 1, #com)
        end

        returnvalue = ""
        if fs.exists("custom_cmds/"..cmd[1]..".lua") then
            returnvalue = assert(loadfile("custom_cmds/"..cmd[1]..".lua"))(args, directory, user, perm_level)
        elseif cmd[1] == "" then
        else
            returnvalue = "Error:Command not found!"
        end

        if returnvalue == nil then
        elseif starts_with(returnvalue, "Error:") then
            print_f("&r"..cmd[1]..": "..returnvalue:sub(7, #returnvalue))
        elseif starts_with(returnvalue, "Cd:") then
            directory = returnvalue:sub(4, #returnvalue)
            change_directory(directory)
        elseif starts_with(returnvalue, "Print:") then
            returnvalue = returnvalue:sub(7, #returnvalue)

            if #returnvalue > 1 then
                print_f("&w"..returnvalue)
            end
        end
    end

    

    command = ""
end

function console()
    while true do
        monitor.setCursorPos(1,52)
        consolePrefix()

        while true do
            local event, key, isHeld = os.pullEvent("key")
            if key == 28 then
                print_f("")
                term.setCursorBlink(false)
                commandExecute()
                term.setCursorBlink(true)
				break
			elseif key == 14 then
                backspace()
			elseif disabledKeys(key) == true then
            else
                local event, char = os.pullEvent("char")
                writeLetter(key, char)
            end
        end

        if shutdown_bool == true then
            break
        end
    end
end

function start()
    monitor.clear()
    monitor.setTextScale(0.5)
    monitor.setCursorPos(1,52)
    term.clear()
    term.setCursorPos(1,1)
    term.setCursorBlink(true)
    
    change_directory(directory)
    console()
end

start()
